#-----------------------------------------------------------
# Copyright (C) 2015 Martin Dobias
#-----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#---------------------------------------------------------------------

from PyQt5.QtWidgets import QAction, QMessageBox
from .ellenberg import EllenbergDialog

def classFactory(iface):
    return QgisEllenbergPlugin(iface)

class QgisEllenbergPlugin:
    def __init__(self, iface):
        self.iface = iface

        self.my_dialog = None

    def initGui(self):
        self.action = QAction('El', self.iface.mainWindow())
        self.action.triggered.connect(self.run)
        self.iface.addToolBarIcon(self.action)

    def unload(self):
        self.iface.removeToolBarIcon(self.action)
        del self.action

    def run(self):
        self.my_dialog = EllenbergDialog()
        self.my_dialog.show()

