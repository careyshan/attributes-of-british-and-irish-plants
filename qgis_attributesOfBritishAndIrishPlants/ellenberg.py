import pandas as pd
import os
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import (
  QDialog,
  QCompleter
)
ui_file_path = os.path.join(
    os.path.dirname(__file__),
    'ellenberg_dialog_base.ui'
)
WIDGET, BASE = uic.loadUiType(ui_file_path)

class EllenbergDialog(QDialog, WIDGET):

   def __init__(self, parent = None):
      super().__init__(parent)
      self.setupUi(self)
      template_path = os.path.join(
          os.path.dirname(__file__),
          'sample.csv'
      )
      data = pd.read_csv(template_path, header=0)
      self.dict = data.set_index('Taxon name').T.to_dict('list')
      self.names = [*self.dict]
      self.completer = QCompleter(self.names)
      self.filterLineEdit.setCompleter(self.completer)
      self.filterLineEdit.textChanged.connect(self.getInput)
     
   def getInput(self):
        stringList = ['Light', 'Moisture', 'Reaction', 'Nutrients', 'Salinity']
        newList = []
        outputDisplay = []
        for key in self.dict:
            if key == self.filterLineEdit.text():
                listValue = self.dict[key]
                for v in listValue:
                    newList.append(v)
        for i in range(len(newList)):
            outputDisplay.append(f"{stringList[i]} : {newList[i]}")
        return self.textBrowser.setText("\n".join(outputDisplay))

